/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jiangyc.nbrestart;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.LifecycleManager;

@ActionID(
        category = "File",
        id = "com.jiangyc.nbrestart.RestartAction"
)
@ActionRegistration(
        iconBase = "com/jiangyc/nbrestart/images/Power_Restart_16.png",
        displayName = "#CTL_Restart"
)
@ActionReferences({
    @ActionReference(path = "Menu/File", position = 2700)
    ,
  @ActionReference(path = "Toolbars/File", position = 500)
})
public final class RestartAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        LifecycleManager.getDefault().markForRestart();
        LifecycleManager.getDefault().exit();
    }
}
